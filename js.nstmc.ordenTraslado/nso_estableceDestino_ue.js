/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @ScriptName NSO | Establece destino Orden Traslado
 * @NModuleScope Public
 * @Company Netsoft
 * @Author Néstor Ignacio Bernal Morales
 */

/*
- NombreArchivo: nso_estableceDestino_ue.js
- NombreScript: NSO | Establece destino Orden Traslado
- IdScript: customscript_nso_ue_establece_destino
- Implementación: customdeploy_nso_ue_establece_destino
- Descripción: Script de User Event que realiza una orden de transalado y su fulfill a partir del ItemRecipt y acorde a lo encontrado en el registro NSO|Ubicaciones OT
- Autor: Néstor Ignacio Bernal Morales
- Biblioteca:
- Lenguaje: Javascript
- FechaCreación: Mayo 28 de 2019
*/

const TIPO_IMPORTACION = 6;
const FORMULARIO_IMPORTACION_OT = 122;
const ID_TRANSFER_ORDER = 121;
const TIPO_DE_VENTA = 'custbody_nso_tipo_trans';

define(['N/record', 'N/search','N/runtime'], function (record, search, runtime) {

    /**
     * Obtiene los datos base para la generación de la orden de translado, si el ÍtemRecipt fue generado de una orden de Compra (Purchase Order)
     * ó de una orden de Transferencia (Transfer Order)
     * @param createdFromId Id de la transacción sobre la cual se está creando el ItemRecipt
     * @returns {null|array[Object]} Retorna nulo si no encuentra datos, un arreglo en el cual en la posición cero (0) se enceuntran los objetos con la
     * siguiente información de la transacción sobre la cual se está creando el ItemRecipt:
     * Ubicación (location),
     * Tipo TMC (TIPO_DE_VENTA)
     */
    function obtenerDatosBase(tipoTransaccion, createdFromId) {

        if (tipoTransaccion == search.Type.PURCHASE_ORDER) {
            return search.lookupFields({type: search.Type.PURCHASE_ORDER, id: createdFromId, columns: ['location', TIPO_DE_VENTA]});
        }else if(tipoTransaccion == search.Type.TRANSFER_ORDER){
            return search.lookupFields({type: search.Type.TRANSFER_ORDER, id: createdFromId, columns: ['transferlocation', TIPO_DE_VENTA]});
        }

        return null;
    }

    /**
     * Determina la ubicación a la cual debe ser trasladado(os) el/los item(s), y si se debe generar un Fulfill a la orden de Translado acorde al registro NSO|Ubicaciones OT
     * @param idUbicacionOrigen Id de la ubicación de origen
     * @returns {Result array|null} Null en caso de que esa ubicación de Origen no se encuentre en NSO|Ubicaciones OT, Result[] con los valores:
     *  Ubicación de destino(custrecord_nso_ubicaciones_ot)
     *  Genera Fulfill (custrecord_nso_genera_fulfill_ot)
     */
    function siguienteUbicacion(idUbicacionOrigen) {
        try {

            var destino = search.create({
                type: 'customrecord_nso_ubicaciones_ot',
                columns: [
                    {name: 'custrecord_nso_destino_ot'},
                    {name: 'custrecord_nso_genera_fulfill_ot'}
                ],
                filters: [{name: 'custrecord_nso_ubicaciones_ot', operator: 'anyof', values: [idUbicacionOrigen]}]
            });

            destino = destino.run().getRange({start: 0, end: 1});

            if (destino[0]) {
                return destino[0];
            }

            return null;

        } catch (exception) {
            log.error({title: 'siguienteUbicacion', details: JSON.stringify(exception)});
            return null;
        }
    }

    function generarItemFulfill(idOrdenTranslado, deo, trandate) {
        try{
            if(!idOrdenTranslado){
                return null;
            }

            var itemFulfill = record.transform({
                fromType: record.Type.TRANSFER_ORDER,
                fromId: idOrdenTranslado,
                toType: record.Type.ITEM_FULFILLMENT,
                isDynamic: true
            });

            itemFulfill.setValue({fieldId: 'trandate', value: trandate});
            itemFulfill.setValue({fieldId: 'custbody_nso_n_importacion_deo', value: deo});

            itemFulfill.save();

        }catch(exception){
            log.error({title: 'generarItemFulfill', details: JSON.stringify(exception)});
        }
    }

    /**
     * Crea la orden de transferencia para los items
     * @param context
     */
    function afterSubmit(context) {
        try {

            // Se verifica que se esté creeando el item recipt, de forma contraria no se ejecuta el script
            //if (context.type != context.UserEventType.CREATE) {
            //  return;
            //}

            var itemRecipt = context.newRecord;
            var createdFrom = itemRecipt.getValue({fieldId: 'createdfrom'});
            var resultadoBusqueda = search.lookupFields({type: search.Type.TRANSACTION, id: createdFrom, columns: ['type']});

            var tipoTransaccion = resultadoBusqueda['type'][0].value;

            if(tipoTransaccion == 'PurchOrd') {
                tipoTransaccion = search.Type.PURCHASE_ORDER
            } else if(tipoTransaccion == 'TrnfrOrd') {
                tipoTransaccion = search.Type.TRANSFER_ORDER;
            }

            var datosTransaccionOrigen = obtenerDatosBase(tipoTransaccion, createdFrom);
            log.debug('datosTransaccionOrigen',datosTransaccionOrigen[TIPO_DE_VENTA][0].value);

            // Se verifica que sea una transacción valida para generar la orden de transferencia
            if (!datosTransaccionOrigen) {
                return;
            }

            var tipoTmc = datosTransaccionOrigen[TIPO_DE_VENTA][0].value;
            var ubicacionActual = tipoTransaccion == search.Type.PURCHASE_ORDER ? datosTransaccionOrigen['location'][0].value : datosTransaccionOrigen['transferlocation'][0].value;
            var datosSiguienteUbicacion = siguienteUbicacion(ubicacionActual);
            log.debug('debugtipoTmc',tipoTmc);
            // Se verifica que el campo personalizado Tipo sea Importación y que se pueda enviar a una ubicación destino
            if (tipoTmc != TIPO_IMPORTACION || !datosSiguienteUbicacion) {
                return;
            }

            var currentUser = runtime.getCurrentUser();
            var userDepartment = search.lookupFields({type: search.Type.EMPLOYEE, id: currentUser.id, columns: ['department']});

            var ubicacionDestino = datosSiguienteUbicacion.getValue({name: 'custrecord_nso_destino_ot'});
            var generaFulfill = datosSiguienteUbicacion.getValue({name: 'custrecord_nso_genera_fulfill_ot'});

            // Se crea el registro de la orden del translado
            var ordenTraslado = record.create({type: record.Type.TRANSFER_ORDER, isDynamic: true});
            ordenTraslado.setValue({fieldId: 'customform', value: FORMULARIO_IMPORTACION_OT});
            ordenTraslado.setValue({fieldId: 'subsidiary', value: itemRecipt.getValue({fieldId: 'subsidiary'})});
            ordenTraslado.setValue({fieldId: 'location', value: ubicacionActual});
            ordenTraslado.setValue({fieldId: 'transferlocation', value: ubicacionDestino});
            //ordenTraslado.setValue({fieldId: 'custbody_nso_field_tipo_transaccion', value: ID_TRANSFER_ORDER});
            //custbody_nso_tipo_trans
            ordenTraslado.setValue({fieldId: TIPO_DE_VENTA, value: ID_TRANSFER_ORDER});
            //ordenTraslado.setValue({fieldId: 'custbody_nso_tipo', value: itemRecipt.getValue({fieldId: 'custbody_nso_tipo'})});
            //ordenTraslado.setValue({fieldId: 'custbody_nso_tipo_trans', value: itemRecipt.getValue({fieldId: 'custbody_nso_tipo_trans'})});
            ordenTraslado.setValue({fieldId: 'custbody_nso_transaccionorigen', value: createdFrom});
            ordenTraslado.setValue({fieldId: 'department', value: userDepartment['department'][0].value });

            //04/01/2019 - Se solicita que para las ordenes que generen fulfill se haga herencia de los datos DEO y fecha de Transacción
            if(generaFulfill) {
                ordenTraslado.setValue({fieldId: 'custbody_nso_n_importacion_deo', value: itemRecipt.getValue({fieldId: 'custbody_nso_n_importacion_deo'})});
                ordenTraslado.setValue({fieldId: 'trandate', value: itemRecipt.getValue({fieldId: 'trandate'})});
            }

            if(!generaFulfill){
                ordenTraslado.setValue({fieldId: 'orderstatus', value: 'A'});
            }

            var numeroProductos = itemRecipt.getLineCount({sublistId: 'item'});

            // Se itera para agregar los elementos marcados como recibido a la TransferOrder
            for (var i = 0; i < numeroProductos; i++) {
                // Se verifica que el Item esté marcado como recibido para ser agregado en la TransferOrder
                if (!itemRecipt.getSublistValue({sublistId: 'item', fieldId: 'itemreceive', line: i})) {
                    continue;
                }

                ordenTraslado.selectNewLine({sublistId: 'item'});

                var producto = itemRecipt.getSublistValue({sublistId: 'item', fieldId: 'item', line: i});
                var cantidad = itemRecipt.getSublistValue({sublistId: 'item', fieldId: 'quantity', line: i});

                ordenTraslado.setCurrentSublistValue({sublistId: 'item', fieldId: 'item', value: producto});
                ordenTraslado.setCurrentSublistValue({sublistId: 'item', fieldId: 'quantity', value: cantidad});

                if( itemRecipt.getSublistValue({sublistId: 'item', fieldId: 'isnumbered', line: i}) == 'T' ) {
                    var inventoryDetRecipt = itemRecipt.getSublistSubrecord({sublistId: 'item', fieldId: 'inventorydetail', line: i});

                    var assignmentItems = inventoryDetRecipt.getLineCount({sublistId: 'inventoryassignment'});
                    var invDetOT = ordenTraslado.getCurrentSublistSubrecord({sublistId: 'item', fieldId: 'inventorydetail'});

                    invDetOT.setValue({fieldId: 'item', value: inventoryDetRecipt.getValue({fieldId: 'item'})});
                    invDetOT.setValue({fieldId: 'quantity', value: inventoryDetRecipt.getValue({fieldId: 'quantity'})});

                    for ( var j = 0; j < assignmentItems; j++ ) {
                        // Se obtienen los valores del InventoryDetail del ItemRecipt para ser asignados al InventoryDetail de la TransferOrder
                        var serialNumber = inventoryDetRecipt.getSublistValue({sublistId: 'inventoryassignment', fieldId: 'receiptinventorynumber', line: j});
                        var cantidadRecipt = inventoryDetRecipt.getSublistValue({sublistId: 'inventoryassignment', fieldId: 'quantity', line: j});
                        var binNumber = inventoryDetRecipt.getSublistValue({sublistId: 'inventoryassignment', fieldId: 'binnumber', line: j});

                        // InventoryDetail de la TransferOrder
                        invDetOT.selectNewLine({sublistId: 'inventoryassignment'});
                        invDetOT.setCurrentSublistValue({sublistId: 'inventoryassignment', fieldId: 'receiptinventorynumber', value: serialNumber});
                        invDetOT.setCurrentSublistValue({sublistId: 'inventoryassignment', fieldId: 'binnumber', value: binNumber});
                        invDetOT.setCurrentSublistValue({sublistId: 'inventoryassignment', fieldId: 'quantity', value: cantidadRecipt});
                        invDetOT.commitLine({sublistId: 'inventoryassignment'});
                    }
                }

                ordenTraslado.commitLine({sublistId: 'item'});

            }

            var idOrdenTranslado = ordenTraslado.save();
            log.debug('idOrdenTranslado',idOrdenTranslado);

            if(generaFulfill){
                generarItemFulfill(idOrdenTranslado, ordenTraslado.getValue({fieldId: 'custbody_nso_n_importacion_deo'}),  ordenTraslado.getValue({fieldId: 'trandate'}));
            }

        } catch (exception) {
            log.error({title: 'beforeSubmit', details: 'Exepción: ' + exception});
        }
    }

    return {
        afterSubmit: afterSubmit
    };
});