//----------------------------------------------------------------------//
//Nombre archivo : nso_tmc_ajuste_iva_userevent.js
//Descripción	 : Hace un ajuste a las columnas de iva según un campo de la transacción y una matriz de un registro personalizado
//Autor		 	 : Lina Duarte
//Fecha creación : 10/05/2019
//----------------------------------------------------------------------//

/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @ScriptName NSO|Ajuste de IVA | UserEvent
 * @NModuleScope Public
 * @Company Netsoft
 * @Author Lina Duarte
 */
// var arregloCodigoFactura = [];
// var arregloCodigoNotaCredito = [];

define(['N/record', 'N/search'], function (record, search) {

    function beforeSubmit(context) {
        try {
            var recordPrincipal0 = context.newRecord;
            var recordId = recordPrincipal0.id;
            var recordType = recordPrincipal0.type;
            var nuevoTaxCode = null;
            var arregloBuqueda, arregloAsignacion = [];
            log.debug('debug', 'recordType: ' + recordType);

            var transaccion = record.load({
                id: recordId,
                type: recordType
            });
            // busqueda para sacar en dos arreglos ordenados la pos
            // del iva actual y del iva futuro
            var arreglos = cargarMatrizIva();
            var aplica = transaccion.getValue('custbody_nso_interna');
            log.debug('debug', 'aplica: ' + aplica);
            // Si aplica, es true o está marcado, no reclasifica
            if (aplica == 'F' || aplica == false) {
                arregloBuqueda = arreglos.arregloCodigoFactura;
                arregloAsignacion = arreglos.arregloCodigoNotaCredito;
            }else{
                arregloBuqueda = arreglos.arregloCodigoNotaCredito;
                arregloAsignacion = arreglos.arregloCodigoFactura;
            }
            for (var i = 0; i < transaccion.getLineCount('item'); i++) {
                var codigoImpuestoLinea = transaccion.getSublistValue('item', 'taxcode', i);
                nuevoTaxCode = darCodigoIva(codigoImpuestoLinea, arregloBuqueda, arregloAsignacion );
                if (transaccion.getSublistValue('item', 'taxcode', i) != 5) {
                    transaccion.setSublistValue('item', 'taxcode', i, nuevoTaxCode);
                }
            }
            var idConfirmar = transaccion.save();
            log.debug('borrar', 'idConfirmar: ' + idConfirmar);
        } catch (e) {
            log.error('error', 'Error en afterSubmit: ' + e);
        }
    }

    /**
     * Carga en los arreglos los valores del cuadro del registro
     * personalizado NSO|IVA en Devoluciones
     */
    function cargarMatrizIva() {
        try {
            // busqueda sobre el registro
            // customrecord_nso_iva_devoluciones columnas
            // custrecord_nso_codigo_iva_factura
            // custrecord_nso_codigo_nota_credito
            var searchRes = search.create({
                type: 'customrecord_nso_iva_devoluciones',
                columns: [{
                    name: 'custrecord_nso_codigo_iva_factura'
                }, {
                    name: 'custrecord_nso_codigo_nota_credito'
                }],
                filters: []
            });

            var searchResult = searchRes.run().getRange({
                start: 0,
                end: 1000
            });

            // log.debug('searchresultgetStatus',
            // JSON.stringify(searchResult));
            var arregloCodigoFactura = [];
            var arregloCodigoNotaCredito = [];

            for (var i = 0; i < searchResult.length; i++) {
                var ivaFactura = searchResult[i].getValue('custrecord_nso_codigo_iva_factura');
                arregloCodigoFactura.push(ivaFactura);

                var ivaNotaCredito = searchResult[i].getValue('custrecord_nso_codigo_nota_credito');
                arregloCodigoNotaCredito.push(ivaNotaCredito);
            }
            return {arregloCodigoFactura: arregloCodigoFactura, arregloCodigoNotaCredito: arregloCodigoNotaCredito};
        } catch (e) {
            log.error('error', 'Error en cargarMatrizIva: ' + e);
        }
    }

    function darCodigoIva(codigoImpuestoLinea, arregloBuqueda, arregloAsignacion) {
        try {
            var nuevoCodigo = codigoImpuestoLinea;
            var encontro = false;
            for (var i = 0; i < arregloBuqueda.length && !encontro; i++) {
                if (arregloBuqueda[i] == codigoImpuestoLinea) {
                    encontro = true;
                    nuevoCodigo = arregloAsignacion[i];
                }
            }
            return nuevoCodigo;
        } catch (e) {
            log.error('error', 'Error en darCodigoIva: ' + e);
        }
    }

    return {
        afterSubmit: beforeSubmit
    };
});